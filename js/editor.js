(function () {
    "use strict";
    window.editor = {};
    editor.doc = null;
    editor.init = function (elementId, insertToElementId) {
        if (elementId) {
            editor.elementId = elementId;
        }
        var element = document.getElementById(editor.elementId);

        if (!element) {
            element = document.createElement('div');
            element.setAttribute('id', editor.elementId);
            var insertTo = document.getElementById(insertToElementId);
            if (insertBefore) {
                insertBefore.appendChild(element);
            } else {
                document.body.insertBefore(element, document.body.children[0]);
            }
        }

        var drawing = editor.drawing = SVG(editor.elementId);
        drawing.width(800);
        drawing.height(300);
        //drawing.viewbox(100, 100, 200, 700);
        //drawing.viewbox(-50, -50, 850, 350);
        drawing.viewbox(0, 0, 800, 300);
    };
})();
