(function () {
    "use strict";
    var editor = window.editor || {};
    var doc = window;
    SVG.extend(SVG.Element, {
        tbox: function () {
            var box = this.bbox();
            var ctm = new SVG.Matrix(this);
            var rv = {
                x: box.x * ctm.a + box.y * ctm.c + ctm.e,
                w: box.w * ctm.a + box.h * ctm.c,
                y: box.x * ctm.b + box.y * ctm.d + ctm.f,
                h: box.w * ctm.b + box.h * ctm.d,
            };
            rv.points = [
                new SVG.Point(rv.x, rv.y), // upper right
                new SVG.Point(rv.x + rv.w, rv.y), // upper left
                new SVG.Point(rv.x + rv.w, rv.y + rv.h), // lower left
                new SVG.Point(rv.x, rv.y + rv.h) // lower right
            ];
            return rv;
        }
    });
    var getMouseCoordinates = function (event) {
        return {
            x: event.pageX || event.touches[0].pageX,
            y: event.pageY || event.touches[0].pageY
        };
    }
    $(doc).on('mousedown', function (event) {
        if (event.which != 1) {
            return;
        }
        if (event.target.closest('.menu')) return;
        var node = event.target.closest('.selectable');
        var topBoundingLine = event.target.closest('.top-bounding-line');
        var leftBoundingLine = event.target.closest('.left-bounding-line');
        var rightBoundingLine = event.target.closest('.right-bounding-line');
        var bottomBoundingLine = event.target.closest('.bottom-bounding-line');
        var line = topBoundingLine || leftBoundingLine || rightBoundingLine || bottomBoundingLine;
        if (!line && !event.shiftKey && !event.ctrlKey) {
            $('.selected').each(function () {
                $(this).removeClass('selected');
                this.instance.unselect();
            });
        }
        if (node) {
            if (!$(node).hasClass('selected')) {
                $(node).addClass('selected');
                node.instance.doselect();
            }
            if ($(node).hasClass('draggable')) {
                $(node).addClass('dragging');
                node.instance.dragstart(getMouseCoordinates(event));
            }
        } else if (line) {
            var node = line.instance.remember('bindedWith').node;
            if ($(node).hasClass('resizable')) {
                if (topBoundingLine) {
                    $(node).addClass('resizingtop');
                } 
                if (leftBoundingLine) {
                    $(node).addClass('resizingleft');
                } 
                if (rightBoundingLine) {
                    $(node).addClass('resizingright');
                } 
                if (bottomBoundingLine) {
                    $(node).addClass('resizingbottom');
                }
                node.instance.resizestart(getMouseCoordinates(event));
            }
        }
        event.preventDefault();
    });
    $(doc).on('mouseup', function (event) {
        $('.dragging, .resizingtop, .resizingleft, .resizingright, .resizingbottom').each(function () {
            this.instance.unselect().doselect();
        });
        $('.dragging').each(function () {
            $(this).removeClass('dragging');
            this.instance.dragend();
        });
        $('.resizingtop, .resizingleft, .resizingright, .resizingbottom').each(function () {
            $(this)
            .removeClass('resizingtop')
            .removeClass('resizingleft')
            .removeClass('resizingright')
            .removeClass('resizingbottom');
            this.instance.resizeend();
        });
        event.preventDefault();
    });
    $(doc).on('mousemove', function (event) {
        $('.dragging, .resizingtop, .resizingleft, .resizingright, .resizingbottom').each(function () {
            this.instance.unselect().doselect();
        });
        $('.dragging').each(function () {
            this.instance.dragmove(getMouseCoordinates(event));
        });
        $('.resizingtop').each(function () {
            this.instance.resizetop(getMouseCoordinates(event));
        });
        $('.resizingleft').each(function () {
            this.instance.resizeleft(getMouseCoordinates(event));
        });
        $('.resizingright').each(function () {
            this.instance.resizeright(getMouseCoordinates(event));
        });
        $('.resizingbottom').each(function () {
            this.instance.resizebottom(getMouseCoordinates(event));
        });
        event.preventDefault();
    });
})();
