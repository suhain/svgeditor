(function () {
    "use strict";
    var editor = window.editor || {};
    var _strokewidth = 5;
    var opacity = 0.5;

    function isSelectionRect(object) {
        return (
            typeof object === 'object'
            && 'l' in object && object.l instanceof SVG.Line
            && 'r' in object && object.r instanceof SVG.Line
            && 't' in object && object.t instanceof SVG.Line
            && 'b' in object && object.b instanceof SVG.Line
        );
    }

    function updateSelectionRect(element, selectionRect) {
        var box = element.tbox();
        var strokewidth = _strokewidth / editor.drawing.viewbox().zoom;

        selectionRect.l
        .attr('x1', box.x)
        .attr('y1', box.y - strokewidth / 2)
        .attr('x2', box.x)
        .attr('y2', box.y + box.h + strokewidth / 2);

        selectionRect.t
        .attr('x1', box.x - strokewidth / 2)
        .attr('y1', box.y)
        .attr('x2', box.x + box.w + strokewidth / 2)
        .attr('y2', box.y);

        selectionRect.r
        .attr('x1', box.x + box.w)
        .attr('y1', box.y - strokewidth / 2)
        .attr('x2', box.x + box.w)
        .attr('y2', box.y + box.h + strokewidth / 2);

        selectionRect.b
        .attr('x1', box.x - strokewidth / 2)
        .attr('y1', box.y + box.h)
        .attr('x2', box.x + box.w + strokewidth / 2)
        .attr('y2', box.y + box.h);
        return selectionRect;
    }

    function drawSelectionRect(element) {
        var box = element.tbox();
        var strokewidth = _strokewidth / editor.drawing.viewbox().zoom;
        return {
            l: editor.drawing.line(box.x, box.y - strokewidth / 2, box.x, box.y + box.h + strokewidth / 2)
            .addTo(element.parent())
            .stroke({width: strokewidth})
            .opacity(opacity)
            .remember('bindedWith', element)
            .attr('class', 'left-bounding-line'),

            t: editor.drawing.line(box.x - strokewidth / 2, box.y, box.x + box.w + strokewidth / 2, box.y)
            .addTo(element.parent())
            .stroke({width: strokewidth})
            .opacity(opacity)
            .remember('bindedWith', element)
            .attr('class', 'top-bounding-line'),

            r: editor.drawing.line(box.x + box.w, box.y - strokewidth / 2, box.x + box.w, box.y + box.h + strokewidth / 2)
            .addTo(element.parent())
            .stroke({width: strokewidth})
            .opacity(opacity)
            .remember('bindedWith', element)
            .attr('class', 'right-bounding-line'),

            b: editor.drawing.line(box.x - strokewidth / 2, box.y + box.h, box.x + box.w + strokewidth / 2, box.y + box.h)
            .addTo(element.parent())
            .stroke({width: strokewidth})
            .opacity(opacity)
            .remember('bindedWith', element)
            .attr('class', 'bottom-bounding-line')
        };
    }

    function destroySelectionRect(selectionRect) {
        for (var side in selectionRect) {
            selectionRect[side].remove();
        }
    }

    SVG.extend(SVG.Element, {
        doselect: function () {
            var selectionRect = this.remember('selectionRect');
            if (!isSelectionRect(selectionRect)) {
                this.drawSelectionRect();
            }
            return this;
        },
        unselect: function () {
            var selectionRect = this.remember('selectionRect');
            if (isSelectionRect(selectionRect)) {
                this.destroySelectionRect();
            }
            return this;
        },

        drawSelectionRect: function () {
            this.remember('selectionRect', drawSelectionRect(this));
            return this;
        },
        updateSelectionRect: function () {
            if (isSelectionRect(this.remember('selectionRect'))) {
                updateSelectionRect(this, this.remember('selectionRect'));
            }
            return this;
        },
        destroySelectionRect: function () {
            destroySelectionRect(this.remember('selectionRect'));
            this.forget('selectionRect');
            return this;
        }
    });
})();
