(function () {
    "use strict";
    function convertCoordinates (coords, node) {
        var point = node.instance.doc().node.createSVGPoint();
        point.x = coords.x;
        point.y = coords.y;
        return point.matrixTransform(node.instance.doc().node.getScreenCTM().inverse());
    }
    SVG.extend(SVG.Element, {
        resizestart: function(initialResizeCoordinates) {
            this.remember('initialResizeCoordinates', initialResizeCoordinates);
        },
        resizetop: function(coordinates) {
            var initialResizeCoordinates = this.remember('initialResizeCoordinates');
            var diffY = convertCoordinates(coordinates, this.node).y - convertCoordinates(initialResizeCoordinates, this.node).y;
            var ctm = new SVG.Matrix(this);
            var box = this.tbox();
            var t1 = new SVG.Matrix().translate(ctm.e, ctm.f + diffY);
            var scale = new SVG.Matrix().scale(1, (box.h - diffY) / box.h);
            var t2 = new SVG.Matrix().translate(-ctm.e, -ctm.f);
            this.matrix(t1.multiply(scale).multiply(t2).multiply(ctm));
            this.remember('initialResizeCoordinates', coordinates);
        },
        resizeleft: function(coordinates) {
            var initialResizeCoordinates = this.remember('initialResizeCoordinates');
            var diffX = convertCoordinates(coordinates, this.node).x - convertCoordinates(initialResizeCoordinates, this.node).x;
            var ctm = new SVG.Matrix(this);
            var box = this.tbox();
            var t1 = new SVG.Matrix().translate(ctm.e + diffX, ctm.f);
            var scale = new SVG.Matrix().scale((box.w - diffX) / box.w, 1);
            var t2 = new SVG.Matrix().translate(-ctm.e, -ctm.f);
            this.matrix(t1.multiply(scale).multiply(t2).multiply(ctm));
            this.remember('initialResizeCoordinates', coordinates);
        },
        resizeright: function(coordinates) {
            var initialResizeCoordinates = this.remember('initialResizeCoordinates');
            var diffX = convertCoordinates(coordinates, this.node).x - convertCoordinates(initialResizeCoordinates, this.node).x;
            var ctm = new SVG.Matrix(this);
            var box = this.tbox();
            var t1 = new SVG.Matrix().translate(ctm.e, ctm.f);
            var scale = new SVG.Matrix().scale((box.w + diffX) / box.w, 1);
            var t2 = new SVG.Matrix().translate(-ctm.e, -ctm.f);
            this.matrix(t1.multiply(scale).multiply(t2).multiply(ctm));
            this.remember('initialResizeCoordinates', coordinates);
        },
        resizebottom: function(coordinates) {
            var initialResizeCoordinates = this.remember('initialResizeCoordinates');
            var diffY = convertCoordinates(coordinates, this.node).y - convertCoordinates(initialResizeCoordinates, this.node).y;
            var ctm = new SVG.Matrix(this);
            var box = this.tbox();
            var t1 = new SVG.Matrix().translate(ctm.e, ctm.f);
            var scale = new SVG.Matrix().scale(1, (box.h + diffY) / box.h);
            var t2 = new SVG.Matrix().translate(-ctm.e, -ctm.f);
            this.matrix(t1.multiply(scale).multiply(t2).multiply(ctm));
            this.remember('initialResizeCoordinates', coordinates);
        },
        resizeend: function () {
            this.forget('initialResizeCoordinates');
            this.fire('resized');
        }
    });
})();
