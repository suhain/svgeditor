(function () {
    "use strict";
    function convertCoordinates (coords, node) {
        var point = node.instance.doc().node.createSVGPoint();
        point.x = coords.x;
        point.y = coords.y;
        return point.matrixTransform(node.getScreenCTM().inverse());
    }
    SVG.extend(SVG.Element, {
        dragstart: function (initialDragCoordinates) {
            this.remember('initialDragCoordinates', initialDragCoordinates);
        },
        dragmove: function (coordinates) {
            var initialDragCoordinates = this.remember('initialDragCoordinates');
            var diffX = convertCoordinates(coordinates, this.node).x - convertCoordinates(initialDragCoordinates, this.node).x;
            var diffY = convertCoordinates(coordinates, this.node).y - convertCoordinates(initialDragCoordinates, this.node).y;
            var ctm = new SVG.Matrix(this);
            var rotation = ctm.extract().rotation;
            ctm = new SVG.Matrix().rotate(-1 * rotation).multiply(ctm);
            var translate = new SVG.Matrix().translate(diffX * ctm.a, diffY * ctm.d);
            this.matrix(new SVG.Matrix().rotate(rotation).multiply(translate.multiply(ctm)));
            this.remember('initialDragCoordinates', coordinates);
        },
        dragend: function () {
            this.forget('initialDragCoordinates');
        }
    });
})();
