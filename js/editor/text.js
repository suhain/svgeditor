(function () {
    "use strict";
    var editor = window.editor || {};

    editor.createText = function (initial_text) {
        var text = editor.drawing.plain(initial_text);
        text.translate(200, 100);
        text.selectable = true;
        text.attr('class', 'selectable resizable draggable');
        return text;
    };
})();
