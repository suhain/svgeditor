(function () {
    window.GUID = guid;
    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
})();
if(typeof(String.prototype.trim) === "undefined") {
    String.prototype.trim = function()  {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}
