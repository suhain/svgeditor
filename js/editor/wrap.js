(function () {
    "use strict";
    var editor = window.editor || {};
    editor.wrap = wrapper;

    function wrapper(node) {
        var element_class = capitalize(node.nodeName);
        try {
            var element = new SVG[element_class];
        } catch(e) {
            throw("No such SVG type '"+element_class+"'");
        }
        element.node = node;
        return element;
    }
    function capitalize(string) {
        while (string.length > 0 && string[0] == "#") string = string.substr(1, string.length - 1);
        if (!string) return string;
        return string[0].toUpperCase() + string.slice(1);
    }
})();
