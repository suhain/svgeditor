(function () {
    "use strict";
    var editor = window.editor || {};

    function getAttributeRecursive(node, attribute) {
        if (node.getAttribute) {
            if (node.getAttribute(attribute)) {
                return node.getAttribute(attribute);
            } else {
                return getAttributeRecursive(node.parentNode, attribute);
            }
        } else {
            return null;
        }
    }

    function replacePercentageWithRealValue(node) {
        if (!node.getAttribute) {
            return;
        }
        function isPercentage(value) {
            value = String(value).trim();
            return value[value.length - 1] == '%';
        }
        function replacePercentage(attrName, parentAttrName) {
            if (isPercentage(node.getAttribute(attrName))) {
                var percentage = parseFloat(node.getAttribute(attrName));
                var parentAttr = getAttributeRecursive(node, parentAttrName);
                if (parentAttr && !isPercentage(parentAttr)) {
                    node.setAttribute(
                        attrName,
                         percentage * parentAttr / 100
                    );
                }
            }
        }
        replacePercentage('x', 'width');
        replacePercentage('y', 'height');
        replacePercentage('cx', 'width');
        replacePercentage('cy', 'height');
    }

    editor.import = function (rawXml) {
        var g = editor.drawing.group().attr('class', 'selectable draggable resizable');
        var xmlDoc = parse(rawXml);
        var x = xmlDoc.documentElement.childNodes;
        var ignored = 0;
        while (x.length > ignored) {
            var node = x[ignored];
            replacePercentageWithRealValue(node);
            try {
                var wrapped = editor.wrap(node);
                if (wrapped.type == "defs") {
                    var defs = editor.drawing.defs();
                    for (var child of wrapped.children()) {
                        defs.add(child);
                    }
                    ignored++;
                } else {
                    g.add(wrapped);
                }
            } catch (err) {
                console.warn(err);
                ignored++;
            }
        }
    };

    function parse(xml) {
        var parser = new DOMParser();
        var doc = parser.parseFromString(xml, "image/svg+xml");
        return doc;
    }

})();
