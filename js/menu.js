(function () {
    "use strict";
    var editor = window.editor || {};

    $('.menu .text').on('click', function () {
        editor.createText('initial text');
    });
    $('.menu .save').on('click', function () {
        $.ajax({
            url: '/save/',
            data: editor.export(),
            contentType: 'application/xml',
            method: "post"
        });
    });
    $('.menu .export').on('click', function () {
        $.ajax({
            url: '/export/',
            data: editor.export(),
            contentType: 'application/xml',
            method: "post",
            success: function (response) {
                var url = response.documentElement.getElementsByTagName('url')[0];
                $('.links').empty();
                $('.links').append("<a href="+url.textContent+">download exported</a>");
            }
        });
    });
    $('.fxg').on('change', function (e) {
        var formData = new FormData();
        var file = e.target.files[0];
        formData.append('file', file);
        $.ajax({
            method: "POST",
            url: "/importfxg/",
            data: formData,
            processData: false,
            cache: false,
            contentType: false,
            error: function (err) {
                var res = JSON.parse(err.responseText);
                $('.links').empty();
                $('.links').append('<span style="color: red;">'+res.detail+'</span>');
            },
            success: function(response) {
                $.ajax({
                    url: response.url,
                    success: function (response, status, xhr) {
                        editor.drawing.clear();
                        editor.import(xhr.responseText);
                    }
                }).then(function (response) {
                    var svg_el = response.getElementsByTagName('svg')[0];
                    var width = svg_el.getAttribute('width');
                    var height = svg_el.getAttribute('height');
                    editor.drawing.viewbox(0, 0, width, height);
                    editor.drawing.width(width);
                    editor.drawing.height(height);
                });
            }
        });
    });
    $('.add-image').on('change', function (e) {
        var formData = new FormData();
        var file = e.target.files[0];
        formData.append('file', file);
        $.ajax({
            method: "PUT",
            url: "/upload/",
            data: formData,
            processData: false,
            cache: false,
            contentType: false,
            success: function(response) {
                if (response.url.endsWith('svg')) {
                    $.ajax({
                        url: response.url,
                        success: function (response, status, xhr) {
                            editor.import(xhr.responseText);
                        }
                    });
                } else {
                    editor.drawing.image(response.url).attr('class', 'selectable draggable resizable');
                }
            }
        });
    });
    $('.front').on('click', function (e) {
        e.preventDefault();
        $('.selected').each(function () {
            this.instance.front();
        });
    });
    $('.back').on('click', function (e) {
        e.preventDefault();
        $('.selected').each(function () {
            this.instance.back();
        });
    });
    $('.remove').on('click', function () {
        $('.selected').each(function () {
            this.instance.unselect().remove();
        });
    });
    $('.opacity-higher').on('click', function () {
        $('.selected').each(function () {
            var currentOpacity = this.instance.opacity();
            if (currentOpacity < 1) {
                currentOpacity += 0.1;
            }
            this.instance.opacity(currentOpacity);
        });
    });
    $('.opacity-lower').click(function () {
        $('.selected').each(function () {
            var currentOpacity = this.instance.opacity();
            if (currentOpacity > 0) {
                currentOpacity -= 0.1;
            }
            this.instance.opacity(currentOpacity);
        });
    });
    $('.group').click(function () {
        var g = editor.drawing.group().attr('class', 'selectable draggable resizable');
        $('.selected').each(function () {
            $(this)
            .removeClass('selected')
            .removeClass('selectable');
            g.add(this.instance.unselect());
        });
        g.doselect();
    });
    $('.ungroup').click(function () {
        $('.selected').each(function () {
            if (this.instance instanceof SVG.G) {
                var g = this.instance;
                $(this)
                .removeClass('selected')
                .removeClass('selectable');
                g.unselect();
                var ctm = new SVG.Matrix(g);
                for (var child of g.children()) {
                    g.parent().add(
                        child
                        .attr('class', 'selectable draggable resizable')
                        .matrix(ctm.multiply(new SVG.Matrix(child)))
                    );
                }
                g.remove();
            }
        });
    });
    $('.font').on('change', function (e) {
        var value = this.value;
        $('.selected').each(function () {
            if (this.instance instanceof SVG.Text) {
                this.instance.attr('font-family', value).unselect().doselect();
                $('.font').css('font-family', value);
            }
        });
    });
})();
