# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-12 16:01
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Maquette',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('modified', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MaquetteLayout',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('index', models.IntegerField(blank=True, default=0)),
                ('maquette', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maquettes.Maquette')),
            ],
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('modified', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('status', models.CharField(blank=True, choices=[('developing', 'developing'), ('ready_to_approve', 'ready to approve'), ('approved', 'approved')], default='developing', max_length=50)),
                ('product_type', models.CharField(choices=[('visiting_card', 'visiting_card')], default='visiting_card', max_length=100)),
                ('pages_count', models.IntegerField(default=1)),
                ('width', models.IntegerField(default=0)),
                ('height', models.IntegerField(default=0)),
                ('chromaticity', models.IntegerField(default=4)),
                ('bleed_size', models.IntegerField(default=5)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TemplateLayout',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('index', models.IntegerField(blank=True, default=0)),
                ('template', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maquettes.Template')),
            ],
        ),
        migrations.AddField(
            model_name='maquette',
            name='template',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maquettes.Template'),
        ),
    ]
