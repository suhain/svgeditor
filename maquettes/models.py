from django.db import models
from django.conf import settings
from .mixins import TimeStamped


class Template(TimeStamped):

    # statuses
    DEVELOPING = 'developing'
    READY_TO_APPROVE = 'ready_to_approve'
    APPROVED = 'approved'

    STATUS_CHOICES = (
        (DEVELOPING, 'developing'), 
        (READY_TO_APPROVE, 'ready to approve'), 
        (APPROVED, 'approved'),
    )

    # product types
    VISITING_CARD = 'visiting_card'
    PRODUCT_TYPE_CHOICES = (
        (VISITING_CARD, 'visiting_card'),
    )

    status = models.CharField(
        max_length=50,
        default=DEVELOPING, 
        choices=STATUS_CHOICES, 
        blank=True
    )

    # preview_url
    creator = models.ForeignKey(settings.AUTH_USER_MODEL)
    # editor
    product_type = models.CharField(
        max_length=100, 
        choices=PRODUCT_TYPE_CHOICES, 
        default=VISITING_CARD
    )
    pages_count = models.IntegerField(default=1)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    chromaticity = models.IntegerField(default=4)
    bleed_size = models.IntegerField(default=5)

    def __str__(self):
        return "%s %sx%s" % (self.product_type, int(self.width), int(self.height))

    def get_absolute_url(self):
        return reverse('api:maquettes-detail', args=(self.pk,))

    @property
    def layouts(self):
        return TemplateLayout.objects.filter(template=self)


class TemplateLayout(models.Model):
    template = models.ForeignKey(Template)
    index = models.IntegerField(default=0, blank=True)

    def __str__(self):
        return "page number %s of template %s" % (self.index, self.template_id)

    def get_absolute_url(self):
        return reverse('api:templates-detail', args=(self.pk,))

    def get_svg_url(self):
        return "%st%s.svg" % (settings.MEDIA_URL, self.pk)


class Maquette(TimeStamped):
    template = models.ForeignKey(Template)
    client = models.ForeignKey(settings.AUTH_USER_MODEL)

    def get_absolute_url(self):
        raise NotImplemented

    @property
    def layouts(self):
        return MaquetteLayout.objects.filter(maquette=self)


class MaquetteLayout(models.Model):
    maquette = models.ForeignKey(Maquette)
    index = models.IntegerField(default=0, blank=True)
    # url
    # preview_url

    def __str__(self):
        return "page number %s of maquette %s" % (self.index, self.maquette_id)

    def get_absolute_url(self):
        raise NotImplemented
