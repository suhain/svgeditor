from rest_framework import viewsets, views
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from django.views.generic import TemplateView
from django.conf import settings
from lxml import etree

from .utils import (
    save_svg, process_uploaded_file, export_svg,
    make_url, import_fxg_zip, process_svg_links,
    get_current_fonts
)
from .parsers import XMLParser
from .renderers import XMLRenderer


class SaveSvgApiView(views.APIView):
    parser_classes = (XMLParser,)
    renderer_classes = (XMLRenderer,)

    def post(self, request):
        path = save_svg(request.data)
        return Response(data=request.data, status=200)


class ExportSvgApiView(views.APIView):
    parser_classes = (XMLParser,)
    renderer_classes = (XMLRenderer,)

    def post(self, request):
        try:
            path = export_svg(etree.tostring(request.data))
        except ValueError as e:
            return Response(data={
                'detail': str(e)
            }, status=400)
        return Response(data={
            'url': request.build_absolute_uri(make_url(path))
        }, status=200)


class UploadApiView(views.APIView):
    parser_classes = (MultiPartParser,)

    def put(self, request):
        try:
            path = process_uploaded_file(request.data['file'])
        except KeyError:
            return Response(
                {'detail': 'Expected file'},
                status=400
            )
        return Response(
            {'url': request.build_absolute_uri(make_url(path))}, 
            status=200
        )


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['fonts'] = get_current_fonts()
        return context


class ImportFxgAPIView(views.APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        try:
            path = process_uploaded_file(request.data['file'])
            path = import_fxg_zip(path)
            process_svg_links(path, request.build_absolute_uri)
        except KeyError:
            return Response(
                {'detail': 'Expected file'},
                status=400
            )
        except ValueError as e: 
            return Response(
                {'detail': str(e)},
                status=400
            )
        return Response(
            {'url': request.build_absolute_uri(make_url(path))}, 
            status=200
        )
