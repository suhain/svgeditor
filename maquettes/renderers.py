from rest_framework.renderers import BaseRenderer
from lxml import etree


class XMLRenderer(BaseRenderer):

    media_type = 'application/xml'
    format = 'xml'
    charset = 'utf-8'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        if data is None:
            return ''

        if isinstance(data, dict):
            data = self._serialize_dict(data)

        return etree.tostring(data)


    def _serialize_dict(self, dictionary):
        def dict2xml(tag, data):
            root = etree.Element(tag)
            if isinstance(data, dict):
                for key in data:
                    root.append(dict2xml(key, data[key]))
            elif isinstance(data, list):
                for item in data:
                    root.append(dict2xml("listitem", item))
            else:
                root.text = str(data)
            return root

        return dict2xml("response", dictionary)


class SVGRenderer(XMLRenderer):
    media_type = 'application/xml+svg'
