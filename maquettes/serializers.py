from rest_framework import serializers
from django.core.urlresolvers import reverse
from . import models
from .mixins import NestedSerializerMixin


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Template
        fields = (
            'id', 'product_type', 'creator', 'pages_count',
            'width', 'height', 'chromaticity',
            'bleed_size', 'created', 'modified',
        )


class TemplateLayoutSerializer(NestedSerializerMixin, serializers.ModelSerializer):

    svg_url = serializers.SerializerMethodField()

    class Meta:
        model = models.TemplateLayout

    def get_svg_url(self, obj):
        build_absolute_uri = lambda uri: uri
        request = self._context.get("request", None)
        if request is not None:
            build_absolute_uri = request.build_absolute_uri
        return build_absolute_uri(reverse('api:templates-layouts-svg', kwargs={
            'parent_lookup_template': obj.template_id,
            'pk': obj.pk,
        }))


class MaquetteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Maquette


class MaquetteLayoutSerializer(NestedSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = models.MaquetteLayout
