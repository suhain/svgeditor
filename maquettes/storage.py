from django.conf import settings
from urllib.parse import urlparse
from io import StringIO


class AbstractStorage(object):

    def read(self, url):
        raise NotImplemented

    def write(self, url, data):
        raise NotImplemented

    def delete(self, url, data):
        raise NotImplemented


class FileSystemStorage(AbstractStorage):

    class Error(Exception):
        pass

    class ReadError(Error):
        pass

    class WriteError(Error):
        pass

    def _get_fs_path(self, url):
        return urlparse(url).path.replace(settings.MEDIA_URL, settings.MEDIA_ROOT)

    def read(self, url, encoding='utf8'):
        fs_path = self._get_fs_path(url)
        try:
            bufferedReader = open(fs_path, 'r', encoding=encoding)
        except (IOError, OSError) as e:
            raise FileSystemStorage.ReadError('File %s not found.' % fs_path) from e
        buffer = StringIO(bufferedReader.read())
        return buffer

    def write(self, url, data, encoding='utf8'):
        fs_path = self._get_fs_path(url)
        try:
            with open(fs_path, 'w', encoding=encoding) as f:
                f.write(data)
        except (IOError, OSError) as e:
            raise FileSystemStorage.WriteError('Unable to write file %s' % fs_path) from e

    def delete(self, url):
        pass


class CloudStorage(AbstractStorage):
    # TODO implement
    pass
