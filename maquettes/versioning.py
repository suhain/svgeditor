from rest_framework.versioning import AcceptHeaderVersioning


class MaquettesAPIVersioning(AcceptHeaderVersioning):
    allowed_versions = ('beta',)
    default_version = 'beta'
