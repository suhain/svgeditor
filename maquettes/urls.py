from rest_framework_extensions.routers import ExtendedSimpleRouter
from django.conf.urls import url, include

from . import views as v
from . import api_views


router = ExtendedSimpleRouter()
router.register(
    'templates', 
    api_views.TemplateViewSet, 
    base_name='templates'

).register(
    'layouts', 
    api_views.TemplateLayoutViewSet, 
    parents_query_lookups=['template'],
    base_name='templates-layouts'
)
router.register(
    'maquettes', 
    api_views.MaquetteViewSet, 
    base_name='maquettes'

).register(
    'layouts', 
    api_views.MaquetteLayoutViewSet, 
    parents_query_lookups=['maquette'],
    base_name='maquettes-layouts'
)


urlpatterns = [
    url(r'^$', v.IndexView.as_view()),
    url(r'^save/$', v.SaveSvgApiView.as_view()),
    url(r'^upload/$', v.UploadApiView.as_view()),
    url(r'^export/$', v.ExportSvgApiView.as_view()),
    url(r'^importfxg/$', v.ImportFxgAPIView.as_view()),

    url(r'^', include(router.urls, namespace='api')),
]
