from rest_framework.parsers import BaseParser
from rest_framework.exceptions import ParseError
from lxml import etree

import logging


logger = logging.getLogger(__name__)


class XMLParser(BaseParser):

    media_type = 'application/xml'

    def parse(self, stream, media_type=None, parser_context=None):
        try:
            tree = etree.fromstring(stream.read())
        except etree.ParseError as e:
            logger.error(e)
            raise ParseError('XML parse error: invalid xml.')
        return tree


class SVGParser(XMLParser):
    media_type = 'application/xml+svg'
    # TODO validate that requested xml is svg
