import cairosvg
import os
import tempfile
import urllib
import shutil
import uuid
import logging
import zipfile
from PIL import Image, ImageFont
from django.conf import settings
from lxml import etree
from fxg2svg.processors.common import NAMESPACEMAP
from fxg2svg.utils.fonts import FontNotFoundError, get_font_alias
from fxg2svg import fxg2svg


logger = logging.getLogger(__name__)


def save_svg(tree, name='file.svg'):
    """
    Save svg file for web view.
    """
    process_svg_ids(tree)
    with open(os.path.join(settings.MEDIA_ROOT, name), 'wb') as f:
        f.write(etree.tostring(tree))
    return os.path.join(settings.MEDIA_ROOT, name)


def process_svg_links(filepath, build_uri):
    svgroot = etree.parse(filepath)
    for e in svgroot.iter('{%s}image' % NAMESPACEMAP[None]):
        href = e.attrib.get('{%s}href' % NAMESPACEMAP['xlink'], '')
        e.set('{%s}href' % NAMESPACEMAP['xlink'], 
              build_uri(os.path.join(settings.MEDIA_URL, href)))
    svgroot.write(os.path.join(settings.MEDIA_ROOT, filepath))


def process_svg_ids(svgroot):
    for e in svgroot.iter():
        id_ = e.attrib.get('id')
        if id_ is not None and id_.lower().startswith("svgjs"):
            e.set('id', str(uuid.uuid4()))


def process_uploaded_file(f):
    filepath = os.path.join(settings.MEDIA_ROOT, f.name)
    with open(filepath, 'wb+') as dest:
        for chunk in f.chunks():
            dest.write(chunk)
    return filepath


def make_url(filepath):
    """
    It is exptected that the file is located in the media folder.
    Returns media url.
    """
    return filepath.replace(settings.MEDIA_ROOT, settings.MEDIA_URL)


def export_svg(svg):
    """
    Archives svg and required resources.
    @svg: string which contain svg data.
    """
    with tempfile.TemporaryDirectory() as dirname:

        svgroot = etree.fromstring(svg)

        for e in svgroot.iter('{%s}image' % NAMESPACEMAP[None]):
            href = e.attrib.get('{%s}href' % NAMESPACEMAP['xlink'], '')
            filename = os.path.basename(href)
            try:
                urllib.request.urlretrieve(href, os.path.join(dirname, filename))
                Image.open(os.path.join(dirname, filename))
            except (urllib.error.HTTPError, ValueError) as e:
                logger.debug(e)
                raise ValueError("Unavailable resourse link %s" % href)
            except IOError:
                logger.debug(e)
                raise ValueError("Unknown image type %s" % href)
            e.set('{%s}href' % NAMESPACEMAP['xlink'], filename)

        process_svg_ids(svgroot)
        svgroot.getroottree().write(os.path.join(dirname, 'file.svg'))
        archivename = os.path.join(settings.MEDIA_ROOT, str(uuid.uuid4()))
        archivemethod = 'zip'
        shutil.make_archive(archivename, archivemethod, dirname)
        return "%s.%s" % (archivename, archivemethod)


def import_fxg_zip(path):
    fxg_filename = None
    cwd = os.getcwd()
    try:
        zip_ref = zipfile.ZipFile(path, 'r')
        for obj in zip_ref.namelist():
            if obj.lower().endswith('.fxg'):
                fxg_filename = obj
                break
        zip_ref.extractall(settings.MEDIA_ROOT)
        svg_filename = "%s.svg" % ".".join(fxg_filename.split('.')[:-1])
        os.chdir(settings.MEDIA_ROOT)
        layouts = fxg2svg(open(os.path.join(settings.MEDIA_ROOT, fxg_filename), 'r'), fontdir=settings.FONTS_ROOT)
        for layout in layouts:
            layout.getroottree().write(
                os.path.join(settings.MEDIA_ROOT, svg_filename)
            )
        zip_ref.close()
        return os.path.join(settings.MEDIA_ROOT, svg_filename)
    except (zipfile.BadZipFile, FontNotFoundError) as e:
        raise ValueError(e)
    finally:
        os.chdir(cwd)


def get_current_fonts():
    fonts = []
    for filename in os.listdir(settings.FONTS_ROOT):
        fontpath = os.path.join(settings.FONTS_ROOT, filename)
        try:
            name = get_font_alias(ImageFont.truetype(fontpath))
        except OSError:
            pass
        else:
            if name is not None:
                fonts.append({
                    'url': os.path.join(settings.FONTS_URL, filename),
                    'name': name,
                })
    return sorted(fonts, key=lambda node: node['name'])
