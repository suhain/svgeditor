from lxml import etree
from fxg2svg.processors.common import NAMESPACEMAP


def create_empty_svg(width, height):
    return etree.Element(
        'svg', 
        width=str(width),
        height=str(height),
        viewBox="0 0 %s %s" % (width, height),
        nsmap=NAMESPACEMAP
    )


def fromstring(stringio):
    print(NAMESPACEMAP)
    return etree.fromstring(stringio.getvalue())


def tostring(svgroot, encoding='unicode'):
    return etree.tostring(svgroot, encoding=encoding)
