from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Template)
admin.site.register(models.Maquette)
admin.site.register(models.TemplateLayout)
admin.site.register(models.MaquetteLayout)
