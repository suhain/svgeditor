from django.apps import AppConfig


class MaquettesConfig(AppConfig):
    name = 'maquettes'
