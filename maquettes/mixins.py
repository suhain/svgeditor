from django.db import transaction, models, IntegrityError
from django.utils import timezone as tz
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.fields import empty
from rest_framework import status

import logging

from .utils.svg import create_empty_svg, fromstring, tostring
from .storage import FileSystemStorage as Storage
from .parsers import XMLParser, SVGParser
from .renderers import XMLRenderer, SVGRenderer


logger = logging.getLogger(__name__)


class TemplateLayoutViewSetMixin(object):

    def perform_create(self, serializer):
        # готовим атомарную транзакцию
        # если по каким то причинам не удастся создать
        # svg разметку, Storage класс сгенерирует исключение
        # которое необходимо обработать именно здесь и послать клиенту ответ с ошибкой сервиса
        try:
            with transaction.atomic():
                # создаем запись в базе данных
                layout = serializer.save()
                # создаем пустую svg разметку
                svgbytes = tostring(create_empty_svg(
                    layout.template.width + 2 * layout.template.bleed_size, 
                    layout.template.height + 2 * layout.template.bleed_size,
                ))
                # записываем разметку в Storage
                Storage().write(layout.get_svg_url(), svgbytes)
                # обновляем нумерацию полос
                for i, l in enumerate(layout.template.layouts):
                    l.index = i
                    l.save()
        except Storage.WriteError as e:
            pass

    def perform_destroy(self, layout):
        template = layout.template
        with transaction.atomic():
            # удаляем полосу
            layout.delete()
            # удаляем разметку
            Storage().delete(layout.get_svg_url())
            # обновляем нумерацию полос
            for index, _layout in enumerate(template.layouts):
                _layout.index = index
                _layout.save()

    @detail_route(
        methods=('get', 'put'),
        parser_classes=(XMLParser, SVGParser),
        renderer_classes=(XMLRenderer, SVGRenderer),
    )
    def svg(self, request, pk=None, parent_lookup_template=None):
        layout = self.get_object()
        if request.method == 'PUT':
            layout = self.get_object()
            svgroot = request.data
            try:
                Storage().write(layout.get_svg_url(), tostring(svgroot))
            except Storage.WriteError as e:
                logger.exception(e)
                return Response(
                    {'detail': 'Service Unavailable'}, 
                    status=status.HTTP_503_SERVICE_UNAVAILABLE
                )
            return Response(svgroot)
        try:
            svgbytes = fromstring(Storage().read(layout.get_svg_url()))
        except Storage.ReadError as e:
            logger.exception(e)
            return Response({'detail': 'Not found'}, status=status.HTTP_404_NOT_FOUND)
        return Response(svgbytes)


class TimeStamped(models.Model):
    """
    Automatically update fields according to the current time and timezone.
    """

    created = models.DateTimeField(blank=True, default=tz.now)
    modified = models.DateTimeField(blank=True, default=tz.now)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created = tz.now()
        self.modified = tz.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class NestedSerializerMixin(object):
    def __init__(self, instance=None, data=empty, **kwargs):
        super(NestedSerializerMixin, self).__init__(instance, data, **kwargs)

        if data is empty:
            # сериализация из orm
            return

        view = self._context.get('view', None)
        if view is not None:
            self.initial_data.update(**view.get_parents_query_dict())
