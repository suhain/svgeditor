from rest_framework.viewsets import ModelViewSet
from rest_framework.renderers import JSONRenderer
from rest_framework_extensions.mixins import NestedViewSetMixin

from .models import Template, TemplateLayout, Maquette, MaquetteLayout
from .serializers import (
    TemplateSerializer, MaquetteSerializer, TemplateLayoutSerializer,
    MaquetteLayoutSerializer,
)
from .mixins import TemplateLayoutViewSetMixin
from .versioning import MaquettesAPIVersioning


class TemplateViewSet(ModelViewSet):
    queryset = Template.objects.all()
    serializer_class = TemplateSerializer
    renderer_classes = (JSONRenderer,)
    versioning_class = MaquettesAPIVersioning


class TemplateLayoutViewSet(NestedViewSetMixin, TemplateLayoutViewSetMixin, ModelViewSet):
    queryset = TemplateLayout.objects.all()
    serializer_class = TemplateLayoutSerializer
    renderer_classes = (JSONRenderer,)
    versioning_class = MaquettesAPIVersioning


class MaquetteViewSet(ModelViewSet):
    queryset = Maquette.objects.all()
    serializer_class = MaquetteSerializer
    renderer_classes = (JSONRenderer,)
    versioning_class = MaquettesAPIVersioning


class MaquetteLayoutViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = MaquetteLayout.objects.all()
    serializer_class = MaquetteLayoutSerializer
    renderer_classes = (JSONRenderer,)
    versioning_class = MaquettesAPIVersioning
