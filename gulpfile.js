var gulp = require('gulp');
var concat = require('gulp-concat');


gulp.task('js', function () {
    return gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/svg.js/dist/svg.js',

        './js/editor.js',
        './js/editor/**/*.js',

        './js/csrf.js',
        './js/menu.js',
        './js/main.js'
    ])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./static/'));
});

gulp.task('css', function () {
    return gulp.src([
        './bower_components/svg.select.js/dist/svg.select.css',
        './css/main.css'
    ])
    .pipe(concat('main.css'))
    .pipe(gulp.dest('./static/'));
});

gulp.task('default', ['js', 'css'], function () {
    gulp.watch(['./js/**/*.js'], ['js']);
    gulp.watch(['./css/**/*.css'], ['css']);
});
